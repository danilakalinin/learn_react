export const updateField = (value) => {
    return {
      type: 'UPDATE_FIELD',
      payload: value
    };
  };
  