const initialState = {
    field: '',
  };
  
  function reducer(state = initialState, action) {
    switch(action.type) {
        case 'UPDATE_FIELD':
         return { ...state, field: action.payload };
      default:
        return state;
    }
  }
  
  export default reducer;
  