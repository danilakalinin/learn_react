import { createStore } from 'redux'
import reducer from '../reducers/textFieldReducers'


const store = createStore(reducer)

export default store