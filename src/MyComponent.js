import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateField } from '../src/redux/actions/textFieldAction';
import { TextField } from '@mui/material';

function MyComponent() {
  const dispatch = useDispatch();
  const fieldValue = useSelector(state => state.field);

  const handleFieldChange = (e) => {
      const value = e.target.value;
      dispatch(updateField(value));
    };
  

  return (
    <div>
      <TextField type="text" value={fieldValue} onChange={handleFieldChange} />
      <TextField type="text" value={fieldValue}/>
    </div>
  );
}

export default MyComponent;
