import { render, screen } from '@testing-library/react';
import React from "react";
import { Provider } from "react-redux";
import '@testing-library/jest-dom'
import store from "./redux/store";
import MyComponent from "./MyComponent";

test('should render text title', () => {
    const text = 'text'
    const title = 'title'

    render(<Provider store={store}><MyComponent text = {text} title = {title} /></Provider>);
    
    expect(screen.getByText('hello')).toBeInTheDocument();
    expect(screen.getByText('title')).toBeInTheDocument();
    
})

test('should render hello text', () => {
    const text = 'text'

    render(<Provider store={store}><MyComponent text = {text} /></Provider>);
   
    expect(screen.getByText('hello')).toBeInTheDocument();
    expect(screen.getByText('text')).toBeInTheDocument();
    

})